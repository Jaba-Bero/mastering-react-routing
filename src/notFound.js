import react from 'react';
import { Link } from "react-router-dom"

function NotFound() {
    return (
        <div className='notfound'>
            <div className='notfound__content '>
                <h2>Page Not found</h2>
                <p>Looks like you've followed a broken link or entered a URL that doesn't <br /> exist on this site.</p>
                <Link to={'/'}>Back to our site</Link>
            </div>

        </div>    
    )
}

export default NotFound
import { useParams } from "react-router-dom"
import React, {useEffect, useState} from 'react';
function User() {
    const params = useParams();
    const userId = params.userId;

    const [users, setUsers] = useState([]);
    const getData = () => {
      const xhr = new XMLHttpRequest();
      xhr.open('get', `http://localhost:5000/community/${userId}`, true);
      xhr.onload = () => {
        const data = JSON.parse(xhr.response);
        setUsers(data);
      };
      xhr.send();
    };
    useEffect(() => {getData()}, []);
    
    return (
        <div key={users.id}  className="community-user first-user center">
            <img src={users.avatar} alt="user avatar" className="community-user__img first-user__img" />
            <p className="community-user__p first-user__p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.</p>
            <span className="community-user__username first-user__username">{users.firstName} {users.lastName}</span>
            <span className="community-user__position first-user__position">{users.position}</span>
        </div>
    )
}

export default User
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Routes, Route} from "react-router-dom"
import Community from './community';
import Join from './join';
import User from './user';
import NotFound from './notFound'
import {BrowserRouter as Router} from "react-router-dom"


function App() {

    return(
        <>
            <Routes >
                <Route path="/" element = {<><Community /> <Join /></>} />
                <Route path='/community' element={<Community />} />
                <Route path={`/community/:userId`} element={<User />} />
                <Route path={'*'} element={<NotFound />}/>
            </Routes >
        </>

    )
}

export default App
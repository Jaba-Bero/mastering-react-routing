import React, {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
// import ReactDOM from 'react-dom/client';
import { Link } from "react-router-dom"

function Community() {
  const dispatch = useDispatch();
  const toggle = useSelector(state => state.toggle);

  const toggleHandler = () => {
    dispatch({ type: 'toggle' })
  }

  const [users, setUsers] = useState([]);
  const getData = () => {
    const xhr = new XMLHttpRequest();
    xhr.open('get', 'http://localhost:5000/community', true);
    xhr.onload = () => {
      const data = JSON.parse(xhr.response);
      setUsers(data);
    };
    xhr.send();
  };
  useEffect(() => {getData()}, []);
  
  const usersRenderer = users.map((user) => (
  <div key={user.id}  className="community-user first-user">
    <img src={user.avatar} alt="user avatar" className="community-user__img first-user__img" />
    <p className="community-user__p first-user__p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.</p>
    <Link to={`/Community/${user.id}`}><span className="community-user__username first-user__username">{user.firstName} {user.lastName}</span></Link>
    <span className="community-user__position first-user__position">{user.position}</span>
  </div>
  ))

  const displayBtn = () => (
   <button className='display-btn' id="btn" onClick={toggleHandler}>{toggle}Hide Section</button>
  )

  return (
    <section className = "community">
      <h2  className="app-title"> Big Community of People Like You</h2>
      {displayBtn()}
      <h3 className="app-subtitle">We’re proud of our products, and we’re really excited
       when we get feedback from our users.</h3>
      <div className="community-users" id='community-users'>
        {usersRenderer}
      </div>
    </section>
  );
}

export default Community;
